
Apresentação
--
Este é um projeto de exemplo no padrão da arquitetura MVC.

Configure um novo virtual host no seu vagrant e importe o [sql do projeto de exemplo](/navegarte-developers/nave-mvc-framework/blob/master/Db-exemplo-MVC.sql)  para ter mais informações, e testar o funcionamento da estrutura.

Você pode ver mais informações no [Wiki do Projeto](/navegarte-developers/nave-mvc-framework/wikis/home)


