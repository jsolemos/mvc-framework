<?php

class Db
{
    private static $db;

    public static function init()
    {
        if (!self::$db)
        {
            try {
                $dsn = 'mysql:host='.config_db_host.';dbname='.config_db_base.';';
                self::$db = new PDO($dsn, config_db_login, config_db_senha,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                die('Connection error: ' . $e->getMessage());
            }
        }
        return self::$db;
    }
}
?>