          <h2 class="sub-header">Contatos</h2>
          <div class="table-responsive">

            <table class="table table-striped">
              <thead>

                <tr>
                  <th width="150"></th>
                  <th>#</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($list_contatos as $row) {?>
                <tr>
                  <td><?php echo $row['COD_CONTATO']; ?></td>
                  <td><?php echo $row['DESC_CATEGORIA']; ?></td>
                  <td><?php echo $row['NOME_CONTATO']; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>