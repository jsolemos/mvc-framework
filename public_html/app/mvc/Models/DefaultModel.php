<?php

class DefaultModel
{
    protected $_db;
    protected $_sql;
    protected $_sqlCount;
    protected $_tabela;
    protected $_qtdPagina = 10;
    protected $_num_pag = 0;
    protected $_results_total = 0;
    protected $_results_parcial = 0;

    public function __construct()
    {
        $this->_db = Db::init();
    }

    public function _setNumPagina($num_pag = 0){
    	$this->_num_pag = $num_pag;
    }

    public function getNumPagina(){
    	return $this->_num_pag;
    }

    public function _setTabela($tabela){
        $this->_tabela = $tabela;
    }


    public function getTabela(){
    	return $this->_tabela;
    }

    public function getResultTotal(){
        return $this->_results_total;
    }

    public function getResultParcial(){
    	return $this->_results_parcial;
    }

    public function _setSql($sql)
    {
        $this->_sql = $sql;
    }

    public function _setSqlCount($sql){
        $this->_sqlCount = $sql;
    }

    public function getQtdPagina(){
    	return $this->_qtdPagina;
    }

    public function _setQtdPagina($qtdPagina = 10){
    	$this->_qtdPagina = $qtdPagina;
    }

    public function getAll($data = null)
    {
        if (!$this->_sql)
        {
            throw new Exception("SQL não foi definido.");
        }

        $sth = $this->_db->prepare($this->_sql);
        $sth->execute($data);
        $this->_results_parcial = $sth->rowCount();

        return $sth->fetchAll();
    }

    public function getPorLimit($limit)
    {
        if (!$this->_sql)
        {
            throw new Exception("SQL não foi definido.");
        }
        $this->_sql .= " LIMIT ?";

        $sth = $this->_db->prepare($this->_sql);
        $sth->bindParam(1, $limit, PDO::PARAM_INT);

        $sth->execute();
        $this->_results_parcial = $sth->rowCount();

        return $sth->fetchAll();
    }

    public function getPorPagina($data = null){
    	if (!$this->_sql)
    	{
    		throw new Exception("SQL não foi definido.");
    	}
    	if (!$this->_sqlCount)
    	{
    		throw new Exception("SQL de contagem não foi definido.");
    	}

    	$sth_t = $this->_db->prepare($this->_sqlCount);
    	$sth_t->execute();
    	$this->_results_total = $sth_t->fetchColumn(0);

    	$this->_sql .= " LIMIT ? , ?";

    	$nInit = $this->_num_pag*$this->getQtdPagina();

    	$sth = $this->_db->prepare($this->_sql);

    	$sth->bindParam(1, $nInit , PDO::PARAM_INT);
    	$sth->bindParam(2, $this->getQtdPagina() , PDO::PARAM_INT);
    	$sth->execute();

    	$this->_results_parcial = $sth->rowCount();

    	return $sth->fetchAll();

    }

    protected function getTypePDOParam($value){

        if(is_numeric($value)){
            return PDO::PARAM_INT;
        }else{
            return PDO::PARAM_STR;
        }
    }

    public function getRow($data = null)
    {
        if (!$this->_sql)
        {
            throw new Exception("SQL não foi definido.");
        }

        $sth = $this->_db->prepare($this->_sql);

        $sth->execute($data);
        $retorno = array();
        $retorno = $sth->fetch();

        return $retorno;
    }
}